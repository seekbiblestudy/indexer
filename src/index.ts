import {Client} from "elasticsearch";

import args from "./indexArgs";

import {connect as stanConnect, Message} from "node-nats-streaming";

import {
    DOWNLOAD_COMPLETE_EVENT_KEY,
    DownloadCompleteEvent
} from "@seekbiblestudy/events.fetch";

import axios from "axios";

import {
    scrape,
    IParsedArticle
}  from "./parser";


import indexContent from "./indexContent";



const elasticsearchUrl = args.elasticsearchUrl;
const natsUrl = args.natsUrl;
const downloadsUrl = args.downloadsUrl;

const queueName = "web-indexer";

const connection = stanConnect(args.natsCluster || "test-cluster", args.natsClientId, natsUrl);

async function getFileByUrl(sha256: string): Promise<string | number> {
    try {
        const response = await axios.get<string>(downloadsUrl + "/" + sha256);
        return response.data;
    } catch (error) {
        const response = error.response;
        console.error("Response", response.status);
        return response.status;
    }
}

connection.on('connect', async()=>{
    const client = new Client({
        host: elasticsearchUrl,
    });

    var opts = connection.subscriptionOptions();
    opts.setDeliverAllAvailable();
    opts.setDurableName(queueName);
    opts.setAckWait(3000);
    opts.setMaxInFlight(10);
    opts.setManualAckMode(true);
    const durableSub = connection.subscribe(DOWNLOAD_COMPLETE_EVENT_KEY, opts);
    console.log("Subscribing to", DOWNLOAD_COMPLETE_EVENT_KEY, "as", args.natsClientId);
    durableSub.on('message', async function(msg: Message) {
        const d = JSON.parse(msg.getData().toString()) as DownloadCompleteEvent;

        const sha256 = Buffer.from(d.sha256, 'base64');
        const hex =  sha256.toString("hex")


        let text: string | number;
        try {
            text = await getFileByUrl(hex);
            if (typeof text === "number") {
                return;
            }
        } catch (err) {
            console.error("Request failed",hex, d.url, err );
            return;
        }
      
        let content: IParsedArticle;

        try {
            content= scrape(text);
        } catch (err) {
            console.error("Failed to parse: " + err);
            msg.ack();
            return;
        }



   try {
    await indexContent(client, d.url, content);
   } catch (err) {
       console.error("Failed to index!");
       console.error(err);
       return;
   }

   console.log("Success", d.url);
       
    
        msg.ack();
      });


});


connection.on('close', function() {
    console.log("Connection closed");
    process.exit();
});

