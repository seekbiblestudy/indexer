import {Client} from "elasticsearch";
import {
    IParsedArticle
} from "./parser";


export default async function indexContent(esClient: Client, url: string, article: IParsedArticle) {
    console.log("Indexed", article.title, "by", article.author, " - ", url);
    await esClient.index({
        index: "articles",
        type: "article",
        body: {url: url, ...article},
    });
}