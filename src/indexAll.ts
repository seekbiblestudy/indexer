import indexContent from "./indexContent";
import {Client} from "elasticsearch";

import {
    scrape,
    IParsedArticle
}  from "./parser";

export async function indexSingle(esClient: Client, text: string, id:string): Promise<void> {
    let content: IParsedArticle;

    content= scrape(text);

   

    await indexContent(esClient, id, content);
}
