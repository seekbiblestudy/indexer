import argparse from "argparse";

const parser = new argparse.ArgumentParser({
    version: '0.0.1',
    addHelp:true,
    description: 'Argparse example'
  });

parser.addArgument(
    [ '-es', '--elasticsearchUrl' ],
    {
      help: 'Elasticsearch connection url'
    }
);

parser.addArgument(
  [ '-nu', '--natsUrl' ],
  {
    help: 'NATs Url'
  }
);

parser.addArgument(
  [ '-nc', '--natsCluster' ],
  {
    help: 'NATs Cluster'
  }
);

parser.addArgument(
  [ '-nid', '--natsClientId' ],
  {
    help: 'NATs Client ID'
  }
);

parser.addArgument(
  [ '-d', '--downloadsUrl' ],
  {
    help: 'URL for Downloads Service'
  }
);

export default parser.parseArgs();