/**
 * Originally authored by Harrison Morgan
 * Converted to typescript by Andrew Maach 3/9/2019
 */

import cheerio from "cheerio";

export interface IParsedArticle {
    title: string;
    author: string;
    content: string;
}

export  function scrape(html_input: string): IParsedArticle {
    let DOM = cheerio.load(html_input);
    let title = DOM('h1 > span');
    let author = DOM('h1 + span');
    if (author.length == 0) {
        author = DOM('h1 + table td + td');
    }
    if (author.length == 0) {
        author = DOM('div#ctl00_cphPage_PubContent_pnlArticleAuthors td:nth-child(2)');
    }
    let content = DOM('p,h2,blockquote');
    if (title.length == 0 || author.length == 0 || content.length < 2) {
        throw new Error(`Error parsing title:${title} author: ${author}`);
    }
    let html = '<h1 id="title">' + title.text() + '</h1>';
    html = html.concat('<h2 id="author">', author.text(), '</h2>');
    for (let i=0; i < content.length; i++) {
        let elem = content[i];
        if (elem.name == 'p') { html = html.concat('<p>', DOM(elem).text(), '</p>'); }
        else if (elem.name == 'h2') { html = html.concat('<h2>', DOM(elem).text(), '</h2>') }
        else if (elem.name == 'blockquote') { html = html.concat('<blockquote>', DOM(elem).text(), '</blockquote>') }
    }
    return {title: title.text(), author: author.text().trim(), content: html};
}
