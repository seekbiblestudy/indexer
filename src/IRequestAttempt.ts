
export function parseResponseName(request: IRequestAttempt): string {
    const hash = Buffer.from(request.ResponseData, "base64");
    return hash.toString("hex");
}

export interface IRequestAttempt {
    Time: string;
    Expires: string;
    Status: number;
    ResponseData: string;
    Approved: boolean;
    Url: string;
}