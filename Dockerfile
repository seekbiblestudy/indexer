FROM node:10 as APPBUILD
COPY src/ /app/src
COPY package.json /app/
COPY package-lock.json /app/
COPY tsconfig.json /app
WORKDIR /app
RUN npm install && npm run build && rm -rf node_modules/
RUN npm install --only=prod
RUN rm -rf src/


FROM node:10
COPY --from=APPBUILD /app /app 
EXPOSE 80
CMD ["node", "/app/dist/index.js", "-es=elasticsearch:9200", "-nu=nats-fetch:4222","-nid=indexer-service" , "-d=http://downloads-service:4002"]
